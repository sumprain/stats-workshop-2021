<!-- #region -->
## Workshop on Introductory Statistics and Research Methodology for Medical Professionals

This repository contains jupyter notebooks created by me to conduct workshop 
on Introductory Statistics and Research Methodology for Medical Professionals.

I will be using Python language for the course.

Following are the contents of the Workshop:

1. **Pre-workshop requirements**
  
  (a) [Prequisites for Workshop.](0-prerequisites.ipynb)

  (b) [Introduction to Python.](10-intro-python.ipynb)


2. **Introductory statistics**

  (a) [Probability Distributions and Normal Distribution.](20-normal-dist.ipynb)

  (b) [Exploratory Data Analysis.](21-eda.ipynb)

  (c) [Inferential Statistics (Hypothesis Testing and Estimation Procedure).](3-inferential-stats.ipynb)

  (d) [Handling Continuous Outcomes.](4-continuous.ipynb)

  (e) [Handling Binary Outcomes.](5-binary.ipynb)

  (f) [Handling Time to Event Outcomes (Survival Analysis).](6-survival.ipynb)


3. **Basics of Research Methodology**

  (a) [Evidence based medicine (EBM): Quality and Strength of Evidence](7-ebm.ipynb)
  
  (b) [Association studies](8-association.ipynb)
  
  (c) [Studies on diagnostic tests](9-diagnostic.ipynb)
  
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/sumprain%2Fstats-workshop-2021/HEAD) to 
access the above contents in editable format, so that you can experiment with 
the features and play around with them.

**I can be contacted at**

1. <sumprain@gmail.com>

2. My website: <https://sumprain.netlify.app>
<!-- #endregion -->
